# RotoTranspose

Naive exploration of a rotary transposition cipher machine. 

# Description: 

Just like it says above. We make a class that has settings such as number of rotors and number of offets with which to create those rotors. 

The class can send a letter of a plaintext through each rotor and then send the output of that text through the next rotor and so on which results in a super tranposition. 

The goal of this code initially was to emulate a rotary cipher machine but we arrived somewhere near learning more how to slice strings and manipulate data storage objects. 

TODO: 

- Recursion maybe? Recursion maybe? Recursion maybe? Recursion maybe? Recursion maybe? Recursion maybe? 
- Novel clean up... a less naive approach? 
- Actually make the offsets shift with each successive input. Like the enigma machine. 

Program output looks like below: 

```
0 bcdefghijklmnopqrstuvwxyza
None
Shift by one character, single rotor.
Length rotors 1
uftu
Expected output is uftu

Send the string test through 2 rotors and each rotor is shifted by one.
0 bcdefghijklmnopqrstuvwxyza
1 cdefghijklmnopqrstuvwxyzab
None
Length rotors 2
Rotor cdefghijklmnopqrstuvwxyzab, iteration 0, output: whvw
whvw

Send the string test through 5 rotors and each rotor is shifted by one.
Length rotors 5
Rotor cdefghijklmnopqrstuvwxyzab, iteration 0, output: whvw
Rotor defghijklmnopqrstuvwxyzabc, iteration 1, output: zkyz
Rotor efghijklmnopqrstuvwxyzabcd, iteration 2, output: docd
Rotor fghijklmnopqrstuvwxyzabcde, iteration 3, output: ithi
ithi
Expected output is ithi

Send the string test through 26 rotors and each rotor is shifted by one, repeats a rotor cycle.
Length rotors 26
Rotor cdefghijklmnopqrstuvwxyzab, iteration 0, output: whvw
Rotor defghijklmnopqrstuvwxyzabc, iteration 1, output: zkyz
Rotor efghijklmnopqrstuvwxyzabcd, iteration 2, output: docd
Rotor fghijklmnopqrstuvwxyzabcde, iteration 3, output: ithi
Rotor ghijklmnopqrstuvwxyzabcdef, iteration 4, output: ozno
Rotor hijklmnopqrstuvwxyzabcdefg, iteration 5, output: vguv
Rotor ijklmnopqrstuvwxyzabcdefgh, iteration 6, output: docd
Rotor jklmnopqrstuvwxyzabcdefghi, iteration 7, output: mxlm
Rotor klmnopqrstuvwxyzabcdefghij, iteration 8, output: whvw
Rotor lmnopqrstuvwxyzabcdefghijk, iteration 9, output: hsgh
Rotor mnopqrstuvwxyzabcdefghijkl, iteration 10, output: test
Rotor nopqrstuvwxyzabcdefghijklm, iteration 11, output: grfg
Rotor opqrstuvwxyzabcdefghijklmn, iteration 12, output: uftu
Rotor pqrstuvwxyzabcdefghijklmno, iteration 13, output: juij
Rotor qrstuvwxyzabcdefghijklmnop, iteration 14, output: zkyz
Rotor rstuvwxyzabcdefghijklmnopq, iteration 15, output: qbpq
Rotor stuvwxyzabcdefghijklmnopqr, iteration 16, output: ithi
Rotor tuvwxyzabcdefghijklmnopqrs, iteration 17, output: bmab
Rotor uvwxyzabcdefghijklmnopqrst, iteration 18, output: vguv
Rotor vwxyzabcdefghijklmnopqrstu, iteration 19, output: qbpq
Rotor wxyzabcdefghijklmnopqrstuv, iteration 20, output: mxlm
Rotor xyzabcdefghijklmnopqrstuvw, iteration 21, output: juij
Rotor yzabcdefghijklmnopqrstuvwx, iteration 22, output: hsgh
Rotor zabcdefghijklmnopqrstuvwxy, iteration 23, output: grfg
Rotor abcdefghijklmnopqrstuvwxyz, iteration 24, output: grfg
grfg
Expected output is grfg
Send the string test through 5 rotors and each rotor is shifted by one.
Length rotors 5
Rotor efghijklmnopqrstuvwxyzabcd, iteration 0, output: uftu
Rotor tuvwxyzabcdefghijklmnopqrs, iteration 1, output: nymn
Rotor xyzabcdefghijklmnopqrstuvw, iteration 2, output: kvjk
Rotor ijklmnopqrstuvwxyzabcdefgh, iteration 3, output: sdrs
sdrs
Expected output is 

```
