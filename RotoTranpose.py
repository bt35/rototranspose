

'''
This class creates a set of rotors with which to encrypt a string.
Parameters:
    numRotors: number of rotors to use.
    offset: The offset of the rotors. This is a list of offsets.

Usage:
    If there are 4 numRotors we must define an offset for each rotor:
    rotors = 4
    offset = [1,26,15,8]

In the case of this class we use 26 character english alphabet so the offset
needs to be within 26.
'''
class RotoTranspose:
    def __init__(self, numRotors, offsets):
        # maybe we need these later.
        #self.numRotors = numRotors
        #self.offsets = offsets
        self.rotors = [None] * numRotors
        # Our chosen alphabet
        self.alpha = "abcdefghijklmnopqrstuvwxyz"

        # create list of rotors and their offets.
        for rotor in range(numRotors):
            self.rotors[rotor] = self.getOffset(self.alpha, rotor, offsets)

    # Get the offsets for line 24.
    def getOffset(self, x, y, offset):
        # Use string slicing to move the alphabets.
        s1 = x[offset[y]:]
        s2 = x[:offset[y]]
        final = s1+s2
        # Concat the two strings together and send it back as the offset alphabet.
        return final

    # Get a single rotary encoding at a time from each different rotor.
    def singleRotorEncode(self, rotor, permutation):
        output = ''
        for k in permutation:
            output += rotor[self.alpha.rfind(k)]

        return output

    # Encoding function.
    def encode(self, rotors, pText):
        # Get the first rotor encoding.
        print("Length rotors", len(rotors))
        # Since this is the first rotor we want to send the plain text to singleRotorEncode
        # and get that output back.
        output = self.singleRotorEncode(rotors[0], pText)

        # All other iterations of the transposed output will go here.
        if len(rotors) > 1:
            # remove the first rotor. We did that operation in line 50.
            # and we do not need that rotor now for the next iterations.
            rotors.remove(rotors[0])
            # For each rotor do the encoding and send the output permutation to the
            # singleRotorEncode method.
            for x in range(len(rotors)):
                output = self.singleRotorEncode(rotors[x], output)
                # This line is for debugging.
                print("Rotor {}, iteration {}, output: {}".format(rotors[x], x, output))
        # Return the output.
        return output

    # Get all possible rotors.
    def getAllRotors(self):
        count = 0
        for i in self.rotors:
            print(count , i)
            count += 1

def test():

    roto = RotoTranspose(1,[1])
    print(roto.getAllRotors())
    print("Shift by one character, single rotor.")
    print(roto.encode(roto.rotors, 'test'))
    print("Expected output is uftu")
    print()
    roto = RotoTranspose(2,[1,2])
    print("Send the string test through 2 rotors and each rotor is shifted by one.")
    print(roto.getAllRotors())
    print(roto.encode(roto.rotors, 'test'))
    print()
    roto = RotoTranspose(5,[1,2,3,4,5])
    print("Send the string test through 5 rotors and each rotor is shifted by one.")
    #print(roto.getAllRotors())
    print(roto.encode(roto.rotors, 'test'))
    print("Expected output is ithi")
    print()
    roto = RotoTranspose(26, [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26])
    print("Send the string test through 26 rotors and each rotor is shifted by one, repeats a rotor cycle.")
    #print(roto.getAllRotors())
    print(roto.encode(roto.rotors, 'test'))
    print("Expected output is grfg")

    roto = RotoTranspose(5,[23,4,19,23,8])
    print("Send the string test through 5 rotors and each rotor is shifted by one.")
    #print(roto.getAllRotors())
    print(roto.encode(roto.rotors, 'test'))
    print("Expected output is ")
    print()
test()

